import React from "react";

const Spinner = () => {
  return (
    <div className="d-flex justified-content-center">
      <div className="spinner-border" role="status"></div>
    </div>
  );
};

export default Spinner;
